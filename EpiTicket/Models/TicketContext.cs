using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace EpiTicket.Models
{
    public class TicketContext : IdentityDbContext<User>
    {
        public TicketContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Association> Associations { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Membership> Membership { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Ticket> Tickets { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Membership>()
                .HasIndex(am => new {am.AssociationId, am.UserId}).IsUnique();

            builder.Entity<User>().ToTable("User");
            builder.Entity<IdentityUserClaim<string>>().ToTable("IdentityUserClaim");
            /*builder.Entity<IdentityRole>().HasData(new IdentityRole { Name = "Admin", NormalizedName = "Admin".ToUpper() });*/
        }

        public static void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        private static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                IdentityRole role = new IdentityRole
                {
                    Name = "Admin"
                };
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
        }

        public static void SeedUsers(UserManager<User> userManager)
        {
            if (userManager.FindByEmailAsync("admin@epiticket.fr").Result == null)
            {
                User user = new User();
                user.FullName = "Sarah Bensalhem";
                user.UserName = "admin@epiticket.fr";
                user.Email = "admin@epiticket.fr";

                IdentityResult result = userManager.CreateAsync
                (user, "123aze").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }

        }
    }
}