using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EpiTicket.Models
{
    public class Association
    {
        [Key] public int AssociationId { get; set; }

        [Required] public string Name { get; set; }

        public List<Event> Events { get; set; }
        public List<Membership> Memberships { get; set; }
    }
}