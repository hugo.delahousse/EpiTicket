using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EpiTicket.Models
{
    public class Ticket
    {
        [Key] public string TicketUUID { get; set; }

        [Required] public string UserId { get; set; }
        public User User { get; set; }

        [Required] public string EventId { get; set; }
        public Event Event { get; set; }
    }
}