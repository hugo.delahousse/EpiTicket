using System.ComponentModel.DataAnnotations;

namespace EpiTicket.Models
{
    public class Membership
    {
        public enum RoleE
        {
            lambda,
            bureau,
            president
        }

        [Key] public int MembershipId { get; set; }
        [Required] public RoleE role { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }

        public int AssociationId { get; set; }
        public Association Association { get; set; }
    }
}