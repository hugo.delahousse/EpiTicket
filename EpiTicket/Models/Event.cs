using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EpiTicket.Models
{
    public class Event
    {
        [Key] public string EventId { get; set; }

        [Required] public string Name { get; set; }

        public string Poster { get; set; }
        public string Description { get; set; }

        [Required] public string Location { get; set; }

        [Required] public uint Capacity { get; set; }

        [Required] public float Price { get; set; }

        [Required] public DateTime StartDate { get; set; }

        [Required] public DateTime EndDate { get; set; }

        [Required]
        public int AssociationId { get; set; }
        public Association Organizer { get; set; }

        public List<Ticket> Tickets { get; set; }
    }
}
