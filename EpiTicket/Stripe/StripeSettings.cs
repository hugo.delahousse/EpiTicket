﻿using System;
namespace EpiTicket.Stripe
{
    public class StripeSettings
    {
        public string SecretKey { get; set; }
        public string PublishableKey { get; set; }

        public StripeSettings()
        {
        }
    }
}
