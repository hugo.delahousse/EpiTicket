﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EpiTicket.Migrations
{
    public partial class Tres : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Membership_User_UserId1",
                table: "Membership");

            migrationBuilder.DropIndex(
                name: "IX_Membership_UserId1",
                table: "Membership");

            migrationBuilder.DropIndex(
                name: "IX_Membership_AssociationId_UserId",
                table: "Membership");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Membership");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Membership",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Membership_UserId",
                table: "Membership",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Membership_AssociationId_UserId",
                table: "Membership",
                columns: new[] { "AssociationId", "UserId" },
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Membership_User_UserId",
                table: "Membership",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Membership_User_UserId",
                table: "Membership");

            migrationBuilder.DropIndex(
                name: "IX_Membership_UserId",
                table: "Membership");

            migrationBuilder.DropIndex(
                name: "IX_Membership_AssociationId_UserId",
                table: "Membership");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Membership",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "Membership",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Membership_UserId1",
                table: "Membership",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Membership_AssociationId_UserId",
                table: "Membership",
                columns: new[] { "AssociationId", "UserId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Membership_User_UserId1",
                table: "Membership",
                column: "UserId1",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
