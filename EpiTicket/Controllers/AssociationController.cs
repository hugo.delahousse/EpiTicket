using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EpiTicket.Models;
using Microsoft.AspNetCore.Authorization;

namespace EpiTicket.Controllers
{
    public class AssociationController : Controller
    {
        private readonly TicketContext _context;
        private readonly User _currentUser;

        private async Task<bool> isAuthorized(int associationId)
        {
            return await _context.Membership.AnyAsync(
                m => m.UserId == _currentUser.Id && m.AssociationId == associationId && m.role != Membership.RoleE.lambda
            );
        } 
        
        public AssociationController(TicketContext context)
        {
            _context = context;
            if (User != null)
            {
                _currentUser = _context.Users.FirstOrDefault(m => m.UserName == User.Identity.Name);
            }
        }

        // GET: Association
        public async Task<IActionResult> Index()
        {
            return View(await _context.Associations.ToListAsync());
        }

        // GET: Association/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Associations
                .Include(a => a.Events)
                .Include(a => a.Memberships)
                    .ThenInclude(membership => membership.User)
                .FirstOrDefaultAsync(m => m.AssociationId == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);
        }

        // GET: Association/AddMembership/5
        [Authorize]
        public async Task<IActionResult> AddMembership(int? id)
        {
            
            if (id == null)
            {
                return NotFound();
            }

            if (!await isAuthorized(id.GetValueOrDefault()) && !User.IsInRole("Admin")) return new ForbidResult();
            
            var association = await _context.Associations
                .Include(a => a.Memberships)
                .FirstOrDefaultAsync(m => m.AssociationId == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);

        }

        // POST: Association/AddMembership/5
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMembership(int id, string name)
        {
            if (!await isAuthorized(id) && !User.IsInRole("Admin")) return new ForbidResult();
            
            if (!ModelState.IsValid) return View();
            
            var user = _context.Users.FirstOrDefault(u => u.UserName == name);
            var membership = new Membership { UserId = user.Id, AssociationId = id };

            _context.Membership.Add(membership);
            await _context.SaveChangesAsync();
                    
            return RedirectToAction(nameof(Index));

        }

        // GET: Association/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Association/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AssociationId,Name")] Association association)
        {
            if (ModelState.IsValid)
            {
                _context.Add(association);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(association);
        }

        // GET: Association/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            
            if (await isAuthorized(id.GetValueOrDefault()) || User.IsInRole("Admin"))
            {
                var association = await _context.Associations.FindAsync(id);
                if (association == null)
                {
                    return NotFound();
                }
                return View(association);
            }

            return new ForbidResult();
        }

        // POST: Association/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AssociationId,Name")] Association association)
        {
            if (id != association.AssociationId)
            {
                return NotFound();
            }

            if (!await isAuthorized(id) && !User.IsInRole("Admin")) return new ForbidResult();
            
            if (!ModelState.IsValid) return View(association);
                
            try
            {
                _context.Update(association);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssociationExists(association.AssociationId))
                {
                    return NotFound();
                }
                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Association/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Associations
                .FirstOrDefaultAsync(m => m.AssociationId == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);
        }

        // POST: Association/Delete/5
        [Authorize (Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var association = await _context.Associations.FindAsync(id);
            _context.Associations.Remove(association);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // POST: Membership/Delete/5
        [Authorize]
        [HttpPost, ActionName("DeleteMembership")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteMembership(int membershipId)
        {
            var membership = await _context.Membership
                .SingleOrDefaultAsync(m => m.MembershipId == membershipId);

            if (membership == null)
            {
                return new ForbidResult();
            }

            var authorized = _context.Membership.Any(
                m => m.AssociationId == membership.AssociationId && m.UserId == _currentUser.Id && m.role == Membership.RoleE.president
            );

            if (!authorized && !User.IsInRole("Admin")) return new ForbidResult();
            
            _context.Membership.Remove(membership);
            _context.Associations.Update(membership.Association);
            await _context.SaveChangesAsync();
            return Redirect($"/Association/Details/{membership.AssociationId}");
        }

        private bool AssociationExists(int id)
        {
            return _context.Associations.Any(e => e.AssociationId == id);
        }
    }
}
