using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using EpiTicket.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EpiTicket.Models;
using Cloudinary = EpiTicket.Data.Cloudinary;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;

namespace EpiTicket.Controllers
{
    public class EventController : Controller
    {
        private readonly TicketContext _context;
        private readonly User _currentUser;
        private readonly Membership _currentUserMembership;
        private readonly IConfiguration _configuration;

        public EventController(TicketContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            if (User != null)
            {
                _currentUser = _context.Users.FirstOrDefault(m => m.UserName == User.Identity.Name);
                _currentUserMembership = _context.Membership.FirstOrDefault(m => m.User.UserName == User.Identity.Name);
            }
        }

        // GET: Event
        public async Task<IActionResult> Index()
        {
            var ticketContext = _context.Events.Include(e => e.Organizer);
            return View(await ticketContext.ToListAsync());
        }

        // GET: Event/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Events
                .Include(e => e.Organizer)
                .FirstOrDefaultAsync(m => m.EventId == id);
            if (@event == null)
            {
                return NotFound();
            }

            ViewData["QRCode"] = QRCodeManager.Base64FromURL(_configuration["BASE_URL"] + Request.Path.Value);
            return View(@event);
        }

        // GET: Event/Create
        [Authorize]
        public IActionResult Create()
        {
            if ((_currentUserMembership != null && _currentUserMembership.role != Membership.RoleE.lambda) || User.IsInRole("Admin"))
            {
                ViewData["AssociationId"] = new SelectList(_context.Associations, "AssociationId", "Name");
                return View();
            }
            else
                return new ForbidResult();
        }

        // POST: Event/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EventId,Name,Poster,Description,Location,Capacity,Price,StartDate,EndDate,AssociationId")] Event @event)
        {
            if ((_currentUserMembership != null && _currentUserMembership.role != Membership.RoleE.lambda) || User.IsInRole("Admin"))
            {
                if (ModelState.IsValid)
                {
                    UploadImage(@event);
                    _context.Add(@event);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                ViewData["AssociationId"] = new SelectList(_context.Associations, "AssociationId", "Name", @event.AssociationId);
                return View(@event);
            }
            else
                return new ForbidResult();
        }

        // GET: Event/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(string id)
        {
            if ((_currentUserMembership != null && _currentUserMembership.role != Membership.RoleE.lambda) || User.IsInRole("Admin"))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var @event = await _context.Events.FindAsync(id);
                if (@event == null)
                {
                    return NotFound();
                }
                ViewData["AssociationId"] = new SelectList(_context.Associations, "AssociationId", "Name", @event.AssociationId);
                return View(@event);
            }
            else
                return new ForbidResult();
        }

        // POST: Event/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("EventId,Name,Poster,Description,Location,Capacity,Price,StartDate,EndDate,AssociationId")] Event @event)
        {
            if (id != @event.EventId)
            {
                return NotFound();
            }

            if ((_currentUserMembership != null && _currentUserMembership.role != Membership.RoleE.lambda) || User.IsInRole("Admin"))
            {
                if (ModelState.IsValid)
                {
                    UploadImage(@event);
                    var existing = await _context.Events.AsNoTracking().FirstOrDefaultAsync(e => e.EventId == @event.EventId);
                    @event.Poster = @event.Poster ?? existing.Poster;
                    try
                    {
                        _context.Update(@event);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!EventExists(@event.EventId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                ViewData["AssociationId"] = new SelectList(_context.Associations, "AssociationId", "Name", @event.AssociationId);
                return View(@event);
            }
            else
                return new ForbidResult();
        }

        // GET: Event/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(string id)
        {
            if ((_currentUserMembership != null && _currentUserMembership.role != Membership.RoleE.lambda) || User.IsInRole("Admin"))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var @event = await _context.Events
                    .Include(e => e.Organizer)
                    .FirstOrDefaultAsync(m => m.EventId == id);
                if (@event == null)
                {
                    return NotFound();
                }

                return View(@event);
            }
            else
                return new ForbidResult();
        }

        // POST: Event/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            if ((_currentUserMembership != null && _currentUserMembership.role != Membership.RoleE.lambda) || User.IsInRole("Admin"))
            {
                var @event = await _context.Events.FindAsync(id);
                _context.Events.Remove(@event);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
                return new ForbidResult();
        }

        private bool EventExists(string id)
        {
            return _context.Events.Any(e => e.EventId == id);
        }
        
        private void UploadImage(Event @event)
        {
            var poster = Request.Form.Files.GetFile("Poster");
            if (poster == null) { return; }

            var response = Cloudinary.Instance.Upload(new ImageUploadParams {
                File = new FileDescription(poster.FileName, poster.OpenReadStream()),
                PublicId = $"EpiTicket/Events/{@event.Name}-poster"
            });

            @event.Poster = response.PublicId;
        }
    }
}
