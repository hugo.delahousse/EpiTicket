﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpiTicket.Models;
using Microsoft.EntityFrameworkCore;

namespace EpiTicket.Controllers
{
    public class HomeController : Controller
    {
        private readonly TicketContext _context;

        public HomeController(TicketContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var ticketContext = await _context.Events.Include(e => e.Organizer).ToListAsync();
            var events = ticketContext.Take(3);
            return View(events);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Events()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
