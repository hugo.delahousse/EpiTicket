﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EpiTicket.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Stripe;

namespace EpiTicket.Controllers
{
    public class PurchaseController : Controller
    {
        private readonly TicketContext _context;

        public PurchaseController(TicketContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        //[Authorize]
        public async Task<IActionResult> Index(string ID)
        {
            var info_event = await _context.Events
                .Include(e => e.Organizer)
                .FirstOrDefaultAsync(e => e.EventId == ID);
            if (info_event == null)
            {
                return Error();
            }
            var user = _context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            ViewData["userName"] = user.FullName;
            ViewData["user"] = user;
            return View(info_event);
        }

        private IActionResult Error()
        {
            return View("Error");
        }

        public async Task<IActionResult> Charge(string stripeEmail, string stripeToken, string ID)
        {
            var info_event = _context.Events.FirstOrDefault(e => e.EventId == ID);
            if (info_event == null)
                return Error();
            var customers = new CustomerService();
            var charges = new ChargeService();

            var customer = customers.Create(new CustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new ChargeCreateOptions
            {
                Amount = Convert.ToInt32(info_event.Price) * 100,
                Description = "Event Purchase",
                Currency = "eur",
                CustomerId = customer.Id
            });

            if (ModelState.IsValid)
            {
                var user = _context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
                var t = new Ticket { UserId = user.Id, EventId = ID };

                _context.Add(t);
                await _context.SaveChangesAsync();

                var ticket = _context.Tickets
                    .Include(tk => tk.User)
                    .Include(tk => tk.Event.Organizer)
                    .FirstOrDefault(tk => tk.TicketUUID == t.TicketUUID);

                return View(ticket);
            }

            return Error();
        }
    }

}
