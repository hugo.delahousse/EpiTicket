﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiTicket.Data;
using EpiTicket.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace EpiTicket.Controllers
{
    public class TicketController : Controller
    {
        private readonly TicketContext _context;
        private readonly IConfiguration _configuration;

        public TicketController(TicketContext context,  IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Detail(string id)
        {
            var ticket = await _context.Tickets
                .Include(t => t.Event.Organizer)
                .Include(t => t.User)
                .FirstOrDefaultAsync(t => t.TicketUUID == id);
            ViewData["QRCode"] = QRCodeManager.Base64FromURL(_configuration["BASE_URL"] + Request.Path.Value);
            return View(ticket);
        }
    }
}
