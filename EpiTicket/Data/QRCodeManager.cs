using System.CodeDom.Compiler;
using System.Drawing;
using QRCoder;

namespace EpiTicket.Data
{
    public class QRCodeManager
    {
        private static readonly Bitmap Logo = new Bitmap("Static/EpiTicketLogo.png");
        public static string Base64FromURL(string url)
        {
            var generator = new QRCodeGenerator();
            var data = generator.CreateQrCode(url, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new Base64QRCode(data);
            return qrCode.GetGraphic(20,
                Color.Black,
                Color.White,
                Logo,
                30,
                12,
                false, 
                Base64QRCode.ImageType.Png
                );
        }
    }
}