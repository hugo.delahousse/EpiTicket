﻿using System;
using System.Collections.Generic;
using System.Linq;
using EpiTicket.Models;

namespace EpiTicket.Data
{
    public class DbInitializer
    {
        public static void InitializeDb(TicketContext context)
        {
            context.Database.EnsureCreated();

            if (context.Events.Any())
                return;
            
            var asso = new Association
            {
                Name = "Nova",
                Events = new List<Event>(),
                Memberships = new List<Membership>()
            };

            var insertAsso = context.Associations.Add(asso);
            context.SaveChanges();
            asso = insertAsso.Entity;

            var events = new List<Event>
            {
                new Event{ Name="Afterwork", Capacity=100, Price=10, EndDate=DateTime.Parse("2005-09-02"), Location="EPITA",StartDate=DateTime.Parse("2005-09-01"), AssociationId = asso.AssociationId },
                new Event{ Name="Novadieu", Capacity=200, Price=5, EndDate=DateTime.Parse("2005-09-05"), Location="EPITA",StartDate=DateTime.Parse("2005-09-04"), AssociationId = asso.AssociationId },
                new Event{ Name="Gala", Capacity=500, Price=7, EndDate=DateTime.Parse("2005-09-10"), Location="EPITA",StartDate=DateTime.Parse("2005-09-10"), AssociationId = asso.AssociationId },
                new Event{ Name="Wanderlust", Capacity=50, Price=15, EndDate=DateTime.Parse("2005-09-02"), Location="EPITA",StartDate=DateTime.Parse("2005-09-01"), AssociationId = asso.AssociationId },
                new Event{ Name="Wandernul", Capacity=100, Price=10, EndDate=DateTime.Parse("2005-09-04"), Location="EPITA",StartDate=DateTime.Parse("2005-09-03"), AssociationId = asso.AssociationId }
            };

            var memberships = new List<Membership>
            {
                new Membership{ User = new User() { UserName = "Nicolas", FullName = "Nicolas"}, AssociationId = asso.AssociationId, Association = asso, UserId = "skfjlkefjezklfj" }
            };

            foreach (Event e in events)
                context.Events.Add(e);

            foreach (Membership m in memberships)
                context.Membership.Add(m);

            context.SaveChanges();
            context.Associations.Update(asso);

            context.SaveChanges();
        }
    }
}
