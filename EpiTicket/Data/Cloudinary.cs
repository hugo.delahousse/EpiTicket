using System.ComponentModel;
using CloudinaryDotNet;
using Microsoft.Extensions.Configuration;

namespace EpiTicket.Data
{
    public class Cloudinary
    {
        public static CloudinaryDotNet.Cloudinary Instance { get; set; }

        public static void Initialize(IConfiguration configuration)
        {
            Instance = new CloudinaryDotNet.Cloudinary(new Account(
                configuration["CLOUDINARY_CLOUD_NAME"],
                configuration["CLOUDINARY_API_KEY"],
                configuration["CLOUDINARY_API_SECRET"]
            ));
        }
    }
}