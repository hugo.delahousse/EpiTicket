FROM mcr.microsoft.com/dotnet/core/sdk:2.2 as build-env

WORKDIR /app

COPY EpiTicket/*.csproj /app

RUN dotnet restore

COPY EpiTicket /app

RUN dotnet publish -c Release -o out

EXPOSE 5000

RUN apt-get update && apt-get install -y libgdiplus libgmp-dev libc6-dev

CMD ["dotnet", "out/EpiTicket.dll"]
