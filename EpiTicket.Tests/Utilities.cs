using System;
using System.Collections.Generic;
using EpiTicket.Models;

namespace EpiTicket.Tests
{
    public class Utilities
    {
        public static void InitializeDbForTests(TicketContext db)
        {
            db.Events.AddRange(GetEvents());
            db.SaveChanges();
        }

        public static List<Association> GetAssos()
        {
            var assos = new List<Association>
            {
                new Association {Name = "Wander"},
                new Association {Name = "Nova"},
                new Association {Name = "Prologin"},
            };

            return assos;
        }

        public static List<Event> GetEvents()
        {
            var assos = GetAssos();

            var events = new List<Event>();
            foreach (var asso in assos)
            {
                var rand = new Random();
                for (var i = 0; i < 10; ++i)
                {
                    events.Add(new Event {
                        Name = $"{asso.Name} event #{i}",
                        Description = $"{asso.Name} event #{i} Description",
                        Location = $"{asso.Name} event #{i} Location",
                        Capacity = (uint) rand.Next(100, 600),
                        Price = rand.Next(15, 105) / 10f,
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now.AddHours(5),
                        Organizer = asso,
                    });
                }
            }

            return events;
        }
    }
}