using System.Threading.Tasks;
using EpiTicket.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;


namespace EpiTicket.Tests.Integration
{
    public class BasicTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public BasicTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory.WithWebHostBuilder(config => config.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                                .AddEntityFrameworkInMemoryDatabase()
                                .BuildServiceProvider();
                services.AddDbContext<TicketContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemory");
                    options.UseInternalServiceProvider(serviceProvider);
                });


                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<TicketContext>();
                    db.Database.EnsureCreated();
                    Utilities.InitializeDbForTests(db);
                }
            }));
        }

        [Theory]
        [InlineData("/")]
        [InlineData("/Home")]
        [InlineData("/Home/Privacy")]
        [InlineData("/Identity/Account/Register")]
        [InlineData("/Identity/Account/Login")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.Equal("text/html; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }

        [Fact]
        public async Task GetEventListHasEventNames()
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync("/Event");
            response.EnsureSuccessStatusCode();
            var responseHtml = await response.Content.ReadAsStringAsync();

            foreach (var e in Utilities.GetEvents())
            {
                Assert.Contains(e.Name, responseHtml);
            }
        }

        [Fact]
        public async Task GetEventListHasEventOrganizer()
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync("/Event");
            response.EnsureSuccessStatusCode();
            var responseHtml = await response.Content.ReadAsStringAsync();

            foreach (var e in Utilities.GetEvents())
            {
                Assert.Contains(e.Organizer.Name, responseHtml);
            }
        }

        [Fact]
        public async Task GetEventCreation()
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync("/Event/Create");
            response.EnsureSuccessStatusCode();
            var responseHtml = await response.Content.ReadAsStringAsync();

            Assert.Contains("input", responseHtml);
        }

        [Fact]
        public async Task GetAssoListHasAssoNames()
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync("/Association");
            response.EnsureSuccessStatusCode();
            var responseHtml = await response.Content.ReadAsStringAsync();

            foreach (var e in Utilities.GetAssos())
            {
                Assert.Contains(e.Name, responseHtml);
            }
        }

        [Fact]
        public async Task GetAssoCreation()
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync("/Association/Create");
            response.EnsureSuccessStatusCode();
            var responseHtml = await response.Content.ReadAsStringAsync();

            Assert.Contains("input", responseHtml);
        }
    }
}